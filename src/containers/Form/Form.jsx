import React, { useEffect } from 'react';
import { useFormik } from 'formik';
import { BootstrapInput } from '../LoginPage';
import { Button } from '@mui/material'
import { variables } from './variable';
import LoadingButton from '@mui/lab/LoadingButton';

import useSelection from 'antd/lib/table/hooks/useSelection';
import { useSelector } from 'react-redux';

const SignupForm = ({ doLogin }) => {
  const loading = useSelector((state) => state.auth.loading)
  const formik = useFormik({
    initialValues: {
      login: '',
      sublogin: '',
      password: '',
    },
    onSubmit: values => {
      alert(loading)
      doLogin(values.login, values.sublogin, values.password)
    },
  });
  return (
    <form onSubmit={formik.handleSubmit} style={{ flexDirection: 'column', display: 'flex' }}>
      <h1>{variables.name}</h1>
      <p style={{ marginBottom: '0px' }}>{variables.login}</p>
      <BootstrapInput
        defaultValue={variables.login}
        id="bootstrap-input"
        value={formik.values.login}
        onChange={formik.handleChange}
        name="login"
        placeholder={variables.sublogin}
      />
      <div style={{ flexDirection: 'row', display: 'flex', justifyContent: 'space-between' }}>
        <p style={{ width: 'fit-content', marginBottom: '0px' }}>{variables.sublogin}</p>
        <p style={{ marginBottom: '0px' }}>{variables.optional}</p>
      </div>
      <BootstrapInput
        id="sublogin"
        onChange={formik.handleChange}
        value={formik.values.sublogin}
        name="sublogin"
      />
      <p style={{ marginBottom: '0px' }}>{variables.password}</p>
      <BootstrapInput
        id="password"
        onChange={formik.handleChange}
        value={formik.values.password}
        name="password"
        placeholder={variables.password}
        type="password"
      />
      {loading ? <LoadingButton
        loading
        loadingIndicator="Loading..."
        variant="contained">
        Fetch data
      </LoadingButton>
        : <Button color="primary"
          variant="contained"
          loading={loading}
          type="submit"
          style={{ width: '140px', marginTop: '15px' }}
        >
          {variables.enter}
        </Button>}
    </form>
  );
};

export default SignupForm
