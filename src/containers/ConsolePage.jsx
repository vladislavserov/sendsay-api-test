import React from "react"
import { useHistory, withRouter } from "react-router"
import { useDispatch, useSelector } from 'react-redux';
import { logout } from 'src/store/actions/auth';

const ConsolePage = ({ history }) => {
    const dispatch = useDispatch();
    const isLoggedIn = useSelector((state) => !!state.auth.sessionKey?.length);
    const doLogout = () => {
        if (isLoggedIn) {
            dispatch(logout())
            history.goBack()
        }
    }
    return (
        <>
            <button onClick={doLogout}>logout</button>
        </>
    )
}

export default withRouter(ConsolePage)