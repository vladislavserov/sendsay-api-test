import React, { useRef, useState } from "react"
import { withRouter } from "react-router"
import { useDispatch, useSelector } from 'react-redux';
import { logout } from 'src/store/actions/auth';
import Header from "./Header";
import SubHeader from "./SubHeader";
import Footer from "./Footer";
import BodySplitPanes from "./SplitPanes";
import { authenticate, authenticateSuccess, authenticateCheck, request } from 'src/store/actions/auth';
// import { requestSaga } from "src/store/sagas/auth";

const ConsolePage = ({ history }) => {
  const state = useSelector((state) => state)
  const currtntRequest = state.requests.currentRequest
  const [isFullScreen, setIsFullScreen] = useState(false)
  const [buttonIsClicked, setButtonIsClicked] = useState(false)

  const [userInfo, setUserInfo] = useState({
    login: state.auth.login,
    // sublogin: state.auth.sublogin.length > 0 ? state.auth.sublogin : 'sublogin'
    sublogin: state.auth.sublogin ? state.auth.sublogin.length > 0 ? state.auth.sublogin : 'sublogin' : 'sublogin'
  })
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state) => !!state.auth.sessionKey?.length);
  const doLogout = () => {
    console.log(userInfo.login, userInfo.sublogin, state.auth.login)
    alert(`inDoLogout, ${isLoggedIn}`)
    dispatch(logout())
    // history.go('/')
    history.push('/')
  }

  const containerRef = useRef()

  const toogleFullScreen = () => {
    if (!document.fullscreenElement) {
      containerRef.current.requestFullscreen().then(setIsFullScreen(true)).catch(err => {
        alert(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`);
      });
    } else {
      document.exitFullscreen().then(setIsFullScreen(false));
    }
  }

  const fullScreevButtonStyle = {
    margin: '10px'
  }

  const headerStyle = {
    height: '50px',
    backgroundColor: '#E5E5E5',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
  const headerProps = {
    containerRef, headerStyle, isFullScreen, toogleFullScreen, fullScreevButtonStyle, userInfo, doLogout
  }

  const sendRequestButtonClickHandler = () => {
    setButtonIsClicked(true)
    setTimeout(() => setButtonIsClicked(false), 2000)
  }

  if (buttonIsClicked) {

    dispatch(
      request(currtntRequest)
    )
  }

  return (
    <>
      <Header headerProps={headerProps} />
      <SubHeader />
      <BodySplitPanes buttonIsClicked={buttonIsClicked} />
      <Footer clickHandler={sendRequestButtonClickHandler} />
    </>
  )
}

export default withRouter(ConsolePage)
