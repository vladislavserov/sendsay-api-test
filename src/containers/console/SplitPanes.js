import styled from 'styled-components';
import React, { useState } from 'react'
import SplitPane from 'react-split-pane';
import InputField from './inputField'

const BodySplitPanes = ({ buttonIsClicked, confirmClick }) => {

    const Wraper = styled.div`
        min-width: 50%;
        display: flex;
        justify-content: center;
        min-height: 100%;
    `

    const localconfirmClick = (text) => {
        alert(text)
    }

    return (
        <SplitPane split="vertical" style={{ position: 'relative' }}>
            <Wraper>
                <div style={{ width: '95%' }}>
                    <p>request:</p>
                    <div style={{ borderRadius: '5px', border: '1px solid rgba(0, 0, 0, 0.21)', width: '100%', height: '90%' }}>
                        <InputField confirmClick={localconfirmClick} buttonIsClicked={buttonIsClicked} />
                    </div>
                </div>
            </Wraper>
            <Wraper>
                <div style={{ width: '95%' }}>
                    <p>reaponce:</p>
                    <div style={{ borderRadius: '5px', border: '1px solid rgba(0, 0, 0, 0.21)', width: '100%', height: '90%' }}>
                        <InputField />
                    </div>
                </div>
            </Wraper>
        </SplitPane>
    )
}

export default BodySplitPanes
