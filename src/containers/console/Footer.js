import React from 'react'
import { Button } from 'antd';
import { AlignLeftOutlined } from '@ant-design/icons'
import { variables } from '../Form/variable';

const Footer = ({ clickHandler }) => {
    return (
        <div style={{ height: '70px', borderTop: '2px solid #7d7d7d80', alignItems: 'center', display: 'flex', paddingLeft: '9px', paddingRight: '9px', justifyContent: 'space-between' }}>
            <Button type="primary" onClick={clickHandler}>{variables.send}</Button>
            <Button type="text">Your GitHab</Button>
            <Button type="text">
                Format
                <AlignLeftOutlined />
            </Button>
        </div>
    )
}

export default Footer
