import React from "react"
import styled from 'styled-components';
import { variables } from "../Form/variable";
import { ExpandOutlined, CompressOutlined, LogoutOutlined } from '@ant-design/icons'

const Header = ({ headerProps }) => {
    const { containerRef, headerStyle, isFullScreen, toogleFullScreen, fullScreevButtonStyle, doLogout, userInfo } = headerProps
    const LogoStyled = styled.img`
`;
    return (
        <div ref={containerRef}>
            <div style={headerStyle}>
                <div style={{ display: 'flex' }}>
                    <LogoStyled src="/icons/logo.svg" alt="" />
                    <h4>{variables.name}</h4>
                </div>
                <div style={{ display: 'flex' }}>
                    <div style={{ display: 'flex', alignItems: 'center', marginRight: '10px' }} onClick={doLogout}>
                        <h3 style={{ marginBottom: '0', marginRight: '4px', border: '2px solid #7d7d7d80', borderRadius: '5px', paddingLeft: '9px', paddingRight: '9px' }}>{userInfo.login + ' :' ?? 'Login :'}{' ' + userInfo.sublogin}</h3>
                        <h3 style={{ marginBottom: '0', marginRight: '4px' }}>Exit</h3>
                        <LogoutOutlined onClick={doLogout} />
                    </div>
                    {isFullScreen ?
                        <CompressOutlined onClick={toogleFullScreen} style={fullScreevButtonStyle} />
                        : <ExpandOutlined onClick={toogleFullScreen} style={fullScreevButtonStyle} />}
                </div>
            </div>
        </div>

    )
}

export default Header
