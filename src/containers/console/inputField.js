import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from 'react-redux';
import { Input } from 'antd';
import useDebounce from "src/helpers/debounce";
import { saveCurrentRequest } from 'src/store/actions/auth';
import Editor from 'react-simple-code-editor';
import { highlight, languages } from 'prismjs/components/prism-core';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-javascript';

const { TextArea } = Input;

const InputField = () => {
  const [requestArea, setRequestArea] = useState(``)

  const dispatch = useDispatch();
  const state = useSelector((state) => state.requests.currentRequest)
  useEffect(() => {
    setRequestArea(state)
  }, [])

  const setText = (value) => {
    const currentRequest = value
    dispatch(
      saveCurrentRequest({ currentRequest })
    )
  }

  return (
    <Editor
      value={requestArea}
      onValueChange={requestArea => setRequestArea(requestArea)}
      bordered={false}
      onMouseLeave={() => {
        if (requestArea !== '') {
          setText(requestArea)
        }
      }
      }
      highlight={requestArea => highlight(requestArea, languages.js)}
      padding={10}
      style={{
        fontFamily: '"Fira code", "Fira Mono", monospace',
        fontSize: 12,
        height: '100%'
      }}
    />
  )
}

export default InputField
