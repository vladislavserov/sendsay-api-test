import React from "react"
import { CloseOutlined } from '@ant-design/icons'

const SubHeader = () => {
    return (
        <div style={{ backgroundColor: '#F6F6F6', display: 'flex', justifyContent: 'sp', alignItems: 'center' }}>
            <div style={{ width: '100%', overflowX: 'hidden' }}>

            </div>
            <div style={{ border: '2px solid #7d7d7d80' }}>
                <CloseOutlined style={{ padding: '9px' }} />
            </div>
        </div>
    )
}

export default SubHeader
