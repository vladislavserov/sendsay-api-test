import { handleActions } from 'redux-actions';

import { ActionTypes } from 'src/store/constants';

export const initialState = {
  loading: false,
  sessionKey: null,
  login: null,
  sublogin: null,
};

export const requestsInitialState = {
  currentRequest: null,
  successRequests: null,
  lastSuccessRequest: null,
}

export default {
  auth: handleActions(
    {
      [ActionTypes.AUTHENTICATE]: (state) => {
        return {
          ...state,
          loading: true,
        };
      },
      [ActionTypes.AUTHENTICATE_SUCCESS]: (state, { payload }) => {
        return {
          ...state,
          loading: false,
          sessionKey: payload.sessionKey,
          login: payload.login,
          sublogin: payload.sublogin,
        };
      },
      [ActionTypes.AUTHENTICATE_FAILURE]: (state) => {
        return {
          ...state,
          sessionKey: null,
          login: null,
          sublogin: null,
          successRequests: null,
        };
      },
      [ActionTypes.LOGOUT]: (state) => {
        return {
          ...state,
          loading: false,
          sessionKey: null,
        };
      },
    },
    initialState
  ),
  requests: handleActions(
    {
      [ActionTypes.SAVE_CURRENT_REQUEST]: (state, { payload }) => {
        return {
          ...state,
          currentRequest: payload.currentRequest,
        };
      },
      [ActionTypes.ADD_SUCCESS_REQUEST]: (state, { payload }) => {
        console.log(payload.payload)
        console.log(state)
        return {
          ...state,
          successRequests: [...state.successRequests, payload.payload]
        };
      },
    },
    requestsInitialState
  )
};
