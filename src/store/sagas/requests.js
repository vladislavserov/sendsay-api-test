
import { all, put, call, takeLatest } from 'redux-saga/effects';
import api from 'src/helpers/sendsay';
import { ActionTypes } from 'src/store/constants';
import { authenticateSuccess, authenticateFailure } from 'src/store/actions/auth';
import { notify } from 'src/containers/LoginPage';

export function* authenticateCheckSaga() {
    try {
        yield api.sendsay.request({
            action: 'ping',
        }).then((responce) => {
            console.log(responce)
        });
    } catch (error) {
        if (error.id === 'error/auth/failed') {
            alert('test request error')
            yield call(logoutSaga);
        }
    }
}

export function* authenticateSaga({ payload }) {
    yield api.sendsay
        .login({
            login: payload.login,
            sublogin: payload.sublogin,
            password: payload.password,
        })
        .then(() => {
            console.log('auth confirm', api.sendsay.session)
            document.cookie = `sendsay_session=${api.sendsay.session}`;
        })
        .catch((err) => {
            notify(err.id)
            document.cookie = '';
            console.log('err', err);
        });

    yield put(
        authenticateSuccess({
            sessionKey: api.sendsay.session,
            login: payload.login,
            sublogin: payload.sublogin,
        }, alert(`after auth success ${payload.login}, ${payload.sublogin}`))
    );
}

export function* logoutSaga() {
    yield put(authenticateFailure());
    document.cookie = '';
}

export default function* root() {
    yield all([
        takeLatest(ActionTypes.AUTHENTICATE, authenticateSaga),
        takeLatest(ActionTypes.AUTHENTICATE_CHECK, authenticateCheckSaga),
        takeLatest(ActionTypes.LOGOUT, logoutSaga),
    ]);
}
