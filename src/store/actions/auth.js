import { createActions } from 'redux-actions';

import { ActionTypes } from 'src/store/constants';

export const { authenticate, request, authenticateSuccess, authenticateCheck, authenticateFailure, logout, saveCurrentRequest, addSuccessRequest } = createActions({
  [ActionTypes.AUTHENTICATE]: (payload) => payload,
  [ActionTypes.AUTHENTICATE_CHECK]: (payload) => payload,
  [ActionTypes.AUTHENTICATE_SUCCESS]: (payload) => payload,
  [ActionTypes.AUTHENTICATE_FAILURE]: (payload) => payload,
  [ActionTypes.LOGOUT]: (payload) => payload,
  [ActionTypes.SAVE_CURRENT_REQUEST]: (payload) => payload,
  [ActionTypes.ADD_SUCCESS_REQUEST]: (payload) => payload,
  [ActionTypes.REQUEST]: (payload) => payload,
});
