import keyMirror from 'fbjs/lib/keyMirror';

export const ActionTypes = keyMirror({
  AUTHENTICATE: undefined,
  AUTHENTICATE_CHECK: undefined,
  AUTHENTICATE_SUCCESS: undefined,
  AUTHENTICATE_FAILURE: undefined,
  LOGOUT: undefined,
  LOGOUT_SUCCESS: undefined,
  LOGOUT_FAILURE: undefined,
  SAVE_CURRENT_REQUEST: undefined,
  ADD_SUCCESS_REQUEST: undefined,
  REQUEST: undefined,
});

export const User = {
  login: '105894007a@gmail.com',
  password: 'yu9Chihog'
}

export const regExp = {
  login: new RegExp('^[_a-zA-Z0-9-+]+(\.[_a-zA-Z0-9-+]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)+$|^([a-zA-Z0-9\w\S]+)'),
  password: new RegExp('^[_a-zA-Z0-9-+]+(\.[_a-zA-Z0-9-+]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)+$|^([a-zA-Z0-9\w\S][^а-яА-Я]+)'),
}
